FROM gitlab/gitlab-runner:v10.3.0

RUN mkdir /opt/bin
WORKDIR /opt/bin

COPY scripts /opt/bin

ENV REGISTER_LOCKED "false"

ENTRYPOINT []
CMD ["/opt/bin/register_run"]
